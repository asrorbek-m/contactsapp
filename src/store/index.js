import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    contacts: [],
  },
  mutations: {
    SET_CONTACTS: (state, contacts) => {
      state.contacts = contacts;
    },
    CREATE_CONTACT: (state, payload) => {
      state.contacts.unshift(payload);
    },
    UPDATE_CONTACT: (state, payload) => {
      let contact = state.contacts.find((contact) => contact.id == payload.id);
      contact = payload;
    },
    DELETE_CONTACT: (state, payload) => {
      let index = state.contacts.indexOf(payload);
      state.contacts.splice(index, 1);
    },
  },
  actions: {
    fetchContacts: ({ commit }) => {
      axios
        .get("http://localhost:3000/contacts/")
        .then((response) => response.data)
        .then((contacts) => {
          commit("SET_CONTACTS", contacts);
        });
    },
    updateContact: (state, payload) => {
      state.commit("UPDATE_CONTACT", payload);
    },
    destroyContact: (state, payload) => {
      state.commit("DELETE_CONTACT", payload);
    },
    createContact: (state, payload) => {
      state.commit("CREATE_CONTACT", payload);
    },
  },
  getters: {
    allContacts: (state) => state.contacts,
    getContact: (state) => (id) => {
      return state.contacts.find((contact) => contact.id === parseInt(id, 10));
    },
    searchContacts: (state) => (keyword) => {
      return state.contacts.filter((contact) =>
        contact.name.toLowerCase().includes(keyword.toLowerCase())
      );
    },
    getNewId: (state) => {
      return state.contacts.length + 1;
    },
  },
});
