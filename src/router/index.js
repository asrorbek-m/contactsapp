import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import ContactDetail from "../views/ContactDetail.vue";
import CreateContact from "../views/CreateContact.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/:id",
    name: "Contact Detail",
    props: true,
    component: ContactDetail,
  },
  {
    path: "/contact/add",
    name: "Create contact",
    component: CreateContact,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
